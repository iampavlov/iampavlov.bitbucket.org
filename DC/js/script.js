
$(document).ready(function () {

var scaleMin = .9;
var scaleDefault = 1;
var scaleMax = 1.1;

$(".Button")
.on( "click", function() {
  $(this).css({"transform": "scale(" + scaleMin + ")"}).delay(100)
  .queue(function (next) {
    $(this).css({"transform": "scale(" + scaleDefault + ")"});
    next();
})
});

$(".Input").on( "click", function() {
  if (!$(this).find("input").val()) {
    $(this).toggleClass('Input-Default Input-Active');
    $(this).children("input").focus();
  }
});

$(".Input").on( "mouseout", function() {
  if (!$(this).find("input").val()) {
    $(this).toggleClass('Input-Default Input-Active');
    $(this).children("input").focus();
  }
});

$(".ChipsList .Chips").on("click", function(){
  $(this).toggleClass('ChipsDefault ChipsActive');
  $(this).siblings().each(function() {
    if ($(this).hasClass("ChipsActive")) {
      $(this).removeClass('ChipsActive').addClass("ChipsDefault");

    }
  });
})


})
