window.onload = function () {

var canvas = document.getElementById('tutorial');
var ctx = canvas.getContext('2d');	

var rightPressed = false;
var leftPressed = false;
var upPressed = false;
var downPressed = false;

// --Pac
var pacX = 45;
var pacY = 45;
var pacDX = 5;
var pacDY = 5;
var pacRadius = 15;
var pacStartPoint = -45;
var pacEndPoint = -315;
var pacCounterClockwise = true;
var pac_pac_counter = 1;
var pacMove = false;
var pacColor = '#E8D958';

// --walls
var wallColor = '#659DFC';

// --fn-degrees to radians
function toRadians(deg) {
    return deg * Math.PI / 180
}

document.addEventListener('keydown', keyDownHandler, false);
document.addEventListener('keyup', keyUpHandler, false);

function keyDownHandler(e) {
    if(e.keyCode == 39) {
        rightPressed = true;
        pacStartPoint = -45;
		pacEndPoint = -315;
    }
    else if(e.keyCode == 38) {
        upPressed = true;
        pacStartPoint = 225;
        pacEndPoint = -35;
    }
    else if(e.keyCode == 37) {
        leftPressed = true;
        pacStartPoint = 135;
        pacEndPoint = -135;
    }
    else if(e.keyCode == 40) {
        downPressed = true;
        pacStartPoint = 55;
        pacEndPoint = -225;
    } 
}

function keyUpHandler(e) {
    if(e.keyCode == 39) {
        rightPressed = false;
    }
    else if(e.keyCode == 38) {
        upPressed = false;
    }
    else if(e.keyCode == 37) {
        leftPressed = false;
    }
    else if(e.keyCode == 40) {
        downPressed = false;
        pacMove = false;
    } 
}

function drawPac(){
	ctx.beginPath();
    ctx.arc(pacX, pacY,pacRadius,toRadians(pacStartPoint),toRadians(pacEndPoint),pacCounterClockwise);
    ctx.lineTo(pacX,pacY);
    ctx.fillStyle = pacColor;
    ctx.fill();
    ctx.closePath();
}

function drawWalls() {
	// окантовка верхняя
	ctx.beginPath();
	ctx.moveTo(0, 270);
	ctx.lineTo(120, 270);
	ctx.lineTo(120, 180);
	ctx.lineTo(30, 180);
	ctx.lineTo(30, 30);
	ctx.lineTo(270, 30);
	ctx.lineTo(270, 90);
	ctx.lineTo(300, 90);
	ctx.lineTo(300, 30);
	ctx.lineTo(540, 30);
	ctx.lineTo(540, 180);
	ctx.lineTo(450, 180);
	ctx.lineTo(450, 270);
	ctx.lineTo(570, 270);
	ctx.lineTo(570, 240);
	ctx.lineTo(480, 240);
	ctx.lineTo(480, 210);
	ctx.lineTo(570, 210);
	ctx.lineTo(570, 0);
	ctx.lineTo(0, 0);
	ctx.lineTo(0, 210);
	ctx.lineTo(90, 210);
	ctx.lineTo(90, 240);
	ctx.lineTo(0, 240);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	//окантовка нижняя
	ctx.beginPath();
	ctx.moveTo(0, 300);
	ctx.lineTo(120, 300);
	ctx.lineTo(120, 390);
	ctx.lineTo(30, 390);
	ctx.lineTo(30, 480);
	ctx.lineTo(60, 480);
	ctx.lineTo(60, 510);
	ctx.lineTo(30, 510);
	ctx.lineTo(30, 600);
	ctx.lineTo(540, 600);
	ctx.lineTo(540, 510);
	ctx.lineTo(510, 510);
	ctx.lineTo(510, 480);
	ctx.lineTo(540, 480);
	ctx.lineTo(540, 390);
	ctx.lineTo(450, 390);
	ctx.lineTo(450, 300);
	ctx.lineTo(570, 300);
	ctx.lineTo(570, 330);
	ctx.lineTo(480, 330);
	ctx.lineTo(480, 360);
	ctx.lineTo(570, 360);
	ctx.lineTo(570, 630);
	ctx.lineTo(0, 630);
	ctx.lineTo(0, 360);
	ctx.lineTo(90, 360);
	ctx.lineTo(90, 330);
	ctx.lineTo(0, 330);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// 60,60,60,30
	ctx.beginPath();
	ctx.rect(60,60,60,30);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// 60,120,60,30
	ctx.beginPath();
	ctx.rect(60,120,60,30);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// 450,60,60,30
	ctx.beginPath();
	ctx.rect(450,120,60,30);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// 450,60,60,30
	ctx.beginPath();
	ctx.rect(450,60,60,30);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// 150,60,90,30
	ctx.beginPath();
	ctx.rect(150,60,90,30);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// 330,60,90,30
	ctx.beginPath();
	ctx.rect(330,60,90,30);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// 390,300,30,90
	ctx.beginPath();
	ctx.rect(390,300,30,90);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// 390,300,30,90
	ctx.beginPath();
	ctx.rect(150,300,30,90);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// 150,420,90,30
	ctx.beginPath();
	ctx.rect(150,420,90,30);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// 330,420,90,30
	ctx.beginPath();
	ctx.rect(330,420,90,30);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// левая-верхняя фиговина
	ctx.beginPath();
	ctx.moveTo(150, 120);
	ctx.lineTo(180, 120);
	ctx.lineTo(180, 180);
	ctx.lineTo(240, 180);
	ctx.lineTo(240, 210);
	ctx.lineTo(180, 210);
	ctx.lineTo(180, 270);
	ctx.lineTo(150, 270);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// центральная-верхняя фиговина
	ctx.beginPath();
	ctx.moveTo(210, 120);
	ctx.lineTo(360, 120);
	ctx.lineTo(360, 150);
	ctx.lineTo(300, 150);
	ctx.lineTo(300, 210);
	ctx.lineTo(270, 210);
	ctx.lineTo(270, 150);
	ctx.lineTo(210, 150);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// правая-верхняя фиговина
	ctx.beginPath();
	ctx.moveTo(390, 120);
	ctx.lineTo(420, 120);
	ctx.lineTo(420, 270);
	ctx.lineTo(390, 270);
	ctx.lineTo(390, 210);
	ctx.lineTo(330, 210);
	ctx.lineTo(330, 180);
	ctx.lineTo(390, 180);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// загон приведений
	ctx.beginPath();
	ctx.moveTo(270, 240);
	ctx.lineTo(210, 240);
	ctx.lineTo(210, 330);
	ctx.lineTo(360, 330);
	ctx.lineTo(360, 240);
	ctx.lineTo(300, 240);
	ctx.lineTo(300, 270);
	ctx.lineTo(330, 270);
	ctx.lineTo(330, 300);
	ctx.lineTo(240, 300);
	ctx.lineTo(240, 270);
	ctx.lineTo(270, 270);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// Дверь
	ctx.beginPath();
	ctx.rect(270,240,30,30);
	ctx.fillStyle = 'hsla(0,0%,100%,0.25)';
	ctx.fill();
	ctx.closePath();
	// центральная фиговина
	ctx.beginPath();
	ctx.moveTo(210, 360);
	ctx.lineTo(360, 360);
	ctx.lineTo(360, 390);
	ctx.lineTo(300, 390);
	ctx.lineTo(300, 450);
	ctx.lineTo(270, 450);
	ctx.lineTo(270, 390);
	ctx.lineTo(210, 390);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// центральная-нижняя фиговина
	ctx.beginPath();
	ctx.moveTo(210, 480);
	ctx.lineTo(360, 480);
	ctx.lineTo(360, 510);
	ctx.lineTo(300, 510);
	ctx.lineTo(300, 570);
	ctx.lineTo(270, 570);
	ctx.lineTo(270, 510);
	ctx.lineTo(210, 510); 
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// левая-нижняя фиговина
	ctx.beginPath();
	ctx.moveTo(60, 540);
	ctx.lineTo(150, 540);
	ctx.lineTo(150, 480);
	ctx.lineTo(180, 480);
	ctx.lineTo(180, 540);
	ctx.lineTo(240, 540);
	ctx.lineTo(240, 570);
	ctx.lineTo(60, 570);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// правая-нижняя фиговина
	ctx.beginPath();
	ctx.moveTo(330, 540);
	ctx.lineTo(390, 540);
	ctx.lineTo(390, 480);
	ctx.lineTo(420, 480);
	ctx.lineTo(420, 540);
	ctx.lineTo(510, 540);
	ctx.lineTo(510, 570);
	ctx.lineTo(330, 570);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// левая Г
	ctx.beginPath();
	ctx.moveTo(60, 420);
	ctx.lineTo(120, 420);
	ctx.lineTo(120, 510);
	ctx.lineTo(90, 510);
	ctx.lineTo(90, 450);
	ctx.lineTo(60, 450);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
	// правая Г
	ctx.beginPath();
	ctx.moveTo(450, 420);
	ctx.lineTo(510, 420);
	ctx.lineTo(510, 450);
	ctx.lineTo(480, 450);
	ctx.lineTo(480, 510);
	ctx.lineTo(450, 510);
	ctx.fillStyle = wallColor;
	ctx.fill();
	ctx.closePath();
}

function drawLine(x, y, h, w) {
	ctx.beginPath();
	ctx.rect(x,y,h,w);
	ctx.fillStyle = '#ffffff';
	ctx.fill();
	ctx.closePath();
	//console.log(x,y);
	//console.log('color = ' + color[2]);
}

function returnColor(x,y,h,w) {

	var color  = ctx.getImageData(x, y, h, w).data;
	function isPositive(number) {
	  return number > 0;
	}
	console.log(color.some(isPositive))
	return color.some(isPositive);
	
}



function draw() {
	ctx.clearRect(pacX-pacRadius, pacY-pacRadius, pacRadius*2, pacRadius*2);
	if(rightPressed && pacX < canvas.width-pacRadius) {
		if (!returnColor(pacX+pacRadius, pacY-pacRadius, 1, pacRadius*2)) {
			pacX += pacDX;
		}
    } 
    else if(upPressed && pacY-45 > 0) {
    	if (!returnColor(pacX-pacRadius, pacY-pacRadius, pacRadius*2, 1)) {
			pacY -= pacDY;
		}
    } 
    else if(downPressed && pacY < canvas.height-45) {
    	if (!returnColor(pacX-pacRadius, pacY+pacRadius, pacRadius*2, 1)) {
    		pacY += pacDY;
    	}
    }
    else if(leftPressed && pacX-pacRadius > 0) {
    	console.log(returnColor(pacX-pacRadius, pacY-pacRadius, 1, pacRadius*2))
    	if (!returnColor(pacX-pacRadius, pacY-pacRadius, 1, pacRadius*2)) {
    		pacX -= pacDX;
    	}
    }

	// --drawAll
	drawPac();
}


// --Pac-pac-pac
setTimeout(function mouth() {
    pacStartPoint+=30*pac_pac_counter;
    pacEndPoint-=30*pac_pac_counter;
    pac_pac_counter*=-1;
    setTimeout(mouth, 100);
}, 100);

setInterval(draw, 20);
drawWalls();

}


