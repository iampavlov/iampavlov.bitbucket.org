$( document ).ready(function() {

// Text input _________________________________

  $(".control input").on("focus", function() {
    var input = $(this);
    var parent = $(this).parent().parent()
    if (!input.hasClass("__focus")) {
        parent.addClass("__focus");
        if (input.val() == "") {
          parent.removeClass("__empty")
        }
    }
  })
  $(".control input").on("focusout", function() {
    var input = $(this);
    var parent = $(this).parent().parent()
    parent.removeClass("__focus")
    if (input.val() == "") {
      parent.addClass("__empty")
    }
  })

// Switcher _________________________________

  $(".switcher").on("click", function(){
    var toggler = $(this)
    toggler.addClass("__loading");
    function switchClass(){
      if (toggler.hasClass("__ON")) {
        toggler.removeClass("__loading").removeClass("__ON").addClass("__OFF")
      } else {
        toggler.removeClass("__loading").removeClass("__OFF").addClass("__ON")
      }
    }
    setTimeout(function(){
      switchClass()
    }, 900)
  })


});
