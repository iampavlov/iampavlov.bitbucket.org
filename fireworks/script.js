window.requestAnimFrame = ( function() {
	return window.requestAnimationFrame ||
		   window.webkitRequestAnimationFrame ||
		   window.mozRequestAnimationFrame ||
		   function( callback ) {
			   window.setTimeout( callback, 1000 / 60 );
		   };
})();

var canvas = document.getElementById( 'canvas' ),
	ctx = canvas.getContext('2d'),
	cw = window.innerWidth,
	ch = window.innerHeight,
	fireworks = [],
	particles = []
	hue = 120,
	limiterTotal = 5,
	limiterTick = 0,
	timerTotal = 80,
	timerTick = 0,
	mousedown = false;
var mx, my;

canvas.width = cw;
canvas.height = ch;

// get a random number within a range
function random (min, max) {

};

// calculate the distance between two points
function calculateDistance(p1x, p1y, p2x, p2y){

};

// create firework
function Firework(sx, sy, tx, ty) {

};

// update firework
Firework.prototype.update = function(index) {

};

// draw firework
Firework.prototype.draw = function() {

};

// create particle
function Particle (x, y) {

};

// update particle
Particle.prototype.update = function (index) {

};

// draw particle
Particle.prototype.draw = function() {

};

// create particle group/explosion 
function createParticles(x, y) {

};

// main demo loop
function loop(){

};

// mouse event bindings
// update the mouse coordinates on mousemove
canvas.addEventListener('mousemove', function(e){

});

// toggle mousedown state and prevent canvas from being selected
canvas.addEventListener('mousedown', function(e){

});

canvas.addEventListener('mouseup', function(e){

});

// once the window loads, we are ready for some fireworks!
window.onload = loop;

